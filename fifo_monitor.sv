class fifo_monitor extends ovm_monitor;
fifo_sequence_item seq_wr;
fifo_sequence_item seq_rd;


virtual fifo_intf intf;
Wrapper wrapper;
ovm_object dummy;

ovm_analysis_port #(fifo_sequence_item) monwrite2sb;
ovm_analysis_port #(fifo_sequence_item) monread2sb;
//registering
`ovm_component_utils(fifo_monitor )

//coverage collector
//covergroup cg;
//???????????????
//endgroup : cg 

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
//cg = new;
endfunction: new


virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 


if($cast(wrapper,dummy))
  begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
monwrite2sb = new("monwrite2sb",this);
monread2sb = new("monread2sb",this);
end
else
ovm_report_info(get_type_name(),"dummy is really dummy",OVM_LOG);

endfunction : build

//integer state = 0,state1= 0; 
bit [1:0] state = 2'b00, state1=2'b00;
 
virtual task run();
forever
begin
writedatatransfer();
  readdatareceived();
end
endtask

  task writedatatransfer ();
 
 case (state)
    
	0: begin
	       @(posedge intf.clk)
	
	       if ( intf.wr_en == 1'b1)
		     begin
			state = 2'b01;
			seq_wr = fifo_sequence_item::type_id::create("seq_wr"); 
		
			
		   end
         
	   end
    1:  begin
	       @(posedge intf.clk)
				//temp_register = temp_register.push_front(intf.rxd);
			seq_wr.data = intf.write_data;
			state = 2'b10;

	
	    end
 
    2:
	    begin
		
		monwrite2sb.write(seq_wr);
		state = 2'b00;

		end
	endcase	
   endtask


task readdatareceived ();
 
 case (state1)
    
	0: begin
	       if ( intf.rd_en == 1'b1)
		     begin
			state1 = 2'b01;
			seq_rd = fifo_sequence_item::type_id::create("seq_rd"); 
		
			
		   end
         
	   end
    1:  begin
	       @(posedge intf.clk)
				//temp_register = temp_register.push_front(intf.rxd);
			seq_rd.data = intf.read_data;

			state1 = 2'b10;

	
	    end
 
    2:
	    begin
		
		monread2sb.write(seq_rd);
		state1 = 2'b00;

		end
		endcase
  endtask

  
  
/*
task writedatatransfer ();
 
monwrite2sb.write(seq_wr);
 
  if (state == 0)
 begin
	if ( intf.wr_en == 1'b1)
		begin
			state = 2'b01;
			seq_wr = fifo_sequence_item::type_id::create("seq_wr"); 
		
			
		end
 end
 
if (state == 1)
begin
@(posedge intf.clk)
//temp_register = temp_register.push_front(intf.rxd);
  seq_wr.data = intf.write_data;
  state = 2'b10;
end

 if (state == 2 )
begin  
monwrite2sb.write(seq_wr);
state = 2'b00;
end 
 
endtask
*/
/*
task readdatareceived ();
if (state1 == 0)
 begin
	if ( intf.rd_en == 1'b1)
		begin
			state1 = 2'b01;
			seq_rd = fifo_sequence_item::type_id::create("seq_rd"); 

			
		end
 end
 
if (state1 == 1)
begin
@(posedge intf.clk)
//temp_register = temp_register.push_front(intf.rxd);
  seq_rd.data = intf.read_data;
  state1 = 2'b10;
end

 if (state1 == 2 )
begin  
monread2sb.write(seq_rd);
state1 = 2'b00;
end 

endtask
 

*/ 
function void model();
$display("dummy model ");
endfunction : model
endclass : fifo_monitorclass fifo_monitor extends ovm_monitor;
fifo_sequence_item seq_wr;
fifo_sequence_item seq_rd;

bit [7:0] data;
bit [3:0] addr;

virtual fifo_intf intf;
Wrapper wrapper;
ovm_object dummy;

ovm_analysis_port #(fifo_sequence_item) monwrite2sb;
ovm_analysis_port #(fifo_sequence_item) monread2sb;
//registering
`ovm_component_utils(fifo_monitor )

//coverage collector
covergroup cg;
	data : coverpoint data;
	addr : coverpoint addr;
endgroup : cg 

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
cg = new;
endfunction: new


virtual function void build();
//casting
	if(!get_config_object("configuration",dummy,0))
		ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
	else
		ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 

	if($cast(wrapper,dummy))
	begin
		ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
		intf = wrapper.intf;
		monwrite2sb = new("monwrite2sb",this);
		monread2sb = new("monread2sb",this);
	end
	else
	ovm_report_info(get_type_name(),"dummy is really dummy",OVM_LOG);
endfunction : build

//integer state = 0,state1= 0; 
bit [1:0] state = 2'b00, state1=2'b00;
 
virtual task run();
	forever
	begin
		writedatatransfer();
		readdatareceived();
	end
endtask

task writedatatransfer ();
 
	case (state)
    
	0: begin
	    @(posedge intf.clk)
	    if ( intf.wr_en == 1'b1)
	     begin
			state = 2'b01;
			seq_wr = fifo_sequence_item::type_id::create("seq_wr"); 
	     end
	   end
	   
    1:  begin
	    @(posedge intf.clk)
		//temp_register = temp_register.push_front(intf.rxd);
		seq_wr.data = intf.write_data;
		seq_wr.addr = intf.write_addr;
		state = 2'b10;
	    end
 
    2:
	    begin
			monwrite2sb.write(seq_wr);
			state = 2'b00;
		end
	endcase	
endtask


task readdatareceived ();
 
    case (state1)
    
	0:  begin
	       if ( intf.rd_en == 1'b1)
		    begin
				state1 = 2'b01;
				seq_rd = fifo_sequence_item::type_id::create("seq_rd"); 
		   end
         
	    end
		
    1:  begin
			@(posedge intf.clk)
			//temp_register = temp_register.push_front(intf.rxd);
			seq_wr.addr = intf.read_addr;
			seq_rd.data = intf.read_data;
    		state1 = 2'b10;
        end
 
    2:
	    begin
			monread2sb.write(seq_rd);
			state1 = 2'b00;
		end
	endcase
endtask

  
  

function void model();
$display("dummy model ");
endfunction : model
endclass : fifo_monitor