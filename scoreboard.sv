class scoreboard extends ovm_component;
`ovm_object_utils(scoreboard)
fifo_sequence_item datafromwrite;
fifo_sequence_item datafromread;
ovm_analysis_export #(fifo_sequence_item) sb_write;
ovm_analysis_export #(fifo_sequence_item) sb_read;
tlm_analysis_fifo #(fifo_sequence_item) getwrite;
tlm_analysis_fifo #(fifo_sequence_item) getread;

function new (string name = "", ovm_component parent = null);
super.new(name,parent);
datafromwrite = new();
datafromread = new();
endfunction : new

function void build();
super.build();
 sb_write= new("sb_write",this);
 sb_read= new("sb_read",this);
 getwrite= new("getwrite",this);
 getread= new("getread",this);
endfunction : build

function void connect();
sb_write.connect(getwrite.analysis_export); //dut
sb_read.connect(getread.analysis_export); //dut
endfunction : connect

task run();
  forever
begin
getwrite.get(datafromwrite);
getread.get(datafromread);
$display("in scoreboard");
 compare(); 
end
endtask: run

function void compare();
 if(datafromwrite.data == datafromread.data)
  ovm_report_info(get_type_name(),"packet matches",OVM_LOG);
 else
  ovm_report_info(get_type_name(),"packet mismatches",OVM_LOG);
 
endfunction : compare
endclass : scoreboard