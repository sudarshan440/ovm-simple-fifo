module fifo(in, out, rd, wr, wraddr,rdaddr,full, empty, reset, clk);
  
  parameter FIFO_WIDTH = 8;
  parameter FIFO_DEPTH = 16;
  parameter FIFO_ADDR_WIDTH = 4;
  
  input [FIFO_WIDTH-1:0] in;
  input [FIFO_ADDR_WIDTH-1:0] wraddr;
  input [FIFO_ADDR_WIDTH-1:0] rdaddr;
  input rd;
  input wr;
  input reset;
  input clk;
  
  output reg [FIFO_WIDTH-1:0] out;
  output reg full;
  output reg empty;
  
  reg [FIFO_WIDTH-1:0] mem [FIFO_DEPTH-1:0];
  
  reg [FIFO_ADDR_WIDTH-1:0] wptr;
  reg [FIFO_ADDR_WIDTH-1:0] rptr;
  
  //assign full = ((wptr+4'h1) == rptr)? 1'b1: 1'b0;
  //assign empty = (wptr == rptr)? 1'b1: 1'b0;
  
  always  @(*) 
  begin
   if (( wptr+4'h1 == rptr))
   begin
     full <= 1;
   end
   
   else
     full <= 0;
  end
  
  always @(*) 
  begin
   if (( wptr  == rptr))
   begin
     empty <= 1;
   end
   
   else
     empty <= 0;
  end
  
  always @ ( full or empty ) begin
    $display( " full = %d empty = %d ", full, empty);
  end
  
  always @( posedge clk or negedge reset) begin
    if(!reset) begin
      wptr <= 0;
      rptr <= 0;
    end else begin
      if(wr && !full) begin
        wptr <= wraddr;
      end      
      if(rd && !empty) begin
        rptr <= rdaddr;
      end
    end
  end
  
  always @( posedge clk or negedge reset) begin
    if(!reset) begin
    end else begin
      if(wr && !full) begin
        mem[wptr] <= in;
        $display("write input is %d, wptr = %d", in, wptr);
      end
    end
  end
  
  always @( posedge clk or negedge reset) begin
    if(!reset) begin
      out <= 0;
    end else if(rd && !empty) begin
      out  <= mem[rptr];
      $display("rd output is %d, rptr = %d", out, rptr);
    end   
  end
  
endmodule