class Wrapper extends ovm_object;
virtual fifo_intf intf;

`ovm_object_utils(Wrapper)

function new(string name = " " );
super.new(name);
endfunction : new

function setVintf(virtual fifo_intf intf);
 this.intf=intf;
endfunction:setVintf 

endclass : Wrapper 