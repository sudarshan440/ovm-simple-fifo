class fifo_sequence_item extends ovm_sequence_item;
  rand byte data;
  rand bit rd_en;
  rand bit wr_en;
  rand bit [3:0] addr;
  `ovm_object_utils(fifo_sequence_item)
  
  function new(string name="");
    super.new(name);
  endfunction
  
  constraint data_c { data >= 0 ;}
  constraint addr_c { addr >= 0 ;}

endclass :fifo_sequence_item