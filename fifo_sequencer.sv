class fifo_sequencer extends ovm_sequencer#(fifo_sequence_item);

//registering
`ovm_component_utils(fifo_sequencer)

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction : new

endclass : fifo_sequencer