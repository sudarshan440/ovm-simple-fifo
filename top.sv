module top;
  import fifoPkg::*;
  import fifoTestPkg::*;

   
  fifo_intf intf();
  //fifo dut(.clk(clk), .wr(intf.wr_en), .in(intf.write_data), .reset(intf.rst), .rd(intf.rd_en), .out(intf.read_data), .full(intf.full), );
  fifo dut ( .in(intf.write_data),
              .out(intf.read_data),
			  .rd(intf.rd_en),
			  .wr(intf.wr_en),
			  .wraddr(intf.write_addr),
			  .rdaddr(intf.read_addr),
 			  .full(intf.full),
			  .empty(intf.empty),
			  .reset(intf.rst),
			  .clk(intf.clk)); 
			  
  initial begin    
      intf.clk = 0;
 	end
	
always #10 intf.clk = ~intf.clk;

     
  

 	
initial
begin
Wrapper wrapper = new("Wrapper");
wrapper.setVintf(intf);

set_config_object("*","configuration",wrapper,0);
run_test("fifo_testcase");

end
endmodule