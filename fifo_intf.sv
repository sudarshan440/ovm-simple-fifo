interface fifo_intf;
  logic clk;
  logic rst;
  logic wr_en;
  logic [7:0] write_data;
  logic [3:0] write_addr;
  logic rd_en;
  logic full;
  logic empty;
  logic [7:0] read_data;
  logic [3:0] read_addr;
endinterface