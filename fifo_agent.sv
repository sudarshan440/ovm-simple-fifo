class fifo_agent extends ovm_agent;
fifo_monitor  write2monitor;
fifo_monitor  readfrommonitor;
fifo_driver driver;
fifo_sequencer sequencer;

ovm_analysis_port #(fifo_sequence_item) writeagent;
ovm_analysis_port #(fifo_sequence_item) readagent;

//registering
`ovm_component_utils(fifo_agent)

//constructor
function new (string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction : new

function void build();
super.build();
writeagent = new("writeagent",this);
readagent = new("readagent",this);

sequencer = fifo_sequencer::type_id::create("sequencer",this);
driver = fifo_driver::type_id::create("driver",this);
write2monitor = fifo_monitor::type_id::create("write2monitor",this);
readfrommonitor = fifo_monitor::type_id::create("readfrommonitor",this);
endfunction : build

function void connect();
driver.seq_item_port.connect(sequencer.seq_item_export);
write2monitor.monwrite2sb.connect(writeagent);
readfrommonitor.monread2sb.connect(readagent);

endfunction : connect


endclass : fifo_agent