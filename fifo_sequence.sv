class fifo_sequence extends ovm_sequence#(fifo_sequence_item);
   fifo_sequence_item seq_item;
   `ovm_object_utils(fifo_sequence)
   
  function new(string name = "");
      super.new(name);
   endfunction
	 
	virtual task body();
	  seq_item = fifo_sequence_item::type_id::create("seq_item");
	  
       repeat(5) begin
	    wait_for_grant( );
        seq_item.randomize() with {rd_en == 0; wr_en == 1;};
		send_request(seq_item);
		wait_for_item_done( );
	   end
	 /*
	   wait_for_grant( );
          seq_item.randomize() with  {rd_en == 1; wr_en == 0;};
		send_request(seq_item) ;
		wait_for_item_done( );
	*/	
		
	endtask
 endclass : fifo_sequence