 class fifo_testcase extends ovm_test;
   
   fifo_sequence seq;
   fifo_sequencer sequencer;
   fifo_environment env;
   
   `ovm_component_utils(fifo_testcase)
   
   function new(string name = " ", ovm_component parent = null);
     super.new(name, parent); 
   endfunction
   
   virtual function void build(   );
       super.build()  ;
      
	   env = fifo_environment::type_id::create("env",this);

   endfunction
   
   
   task run();
$cast(sequencer,env.agent.sequencer);
sequencer.count = 0;
 seq = fifo_sequence::type_id::create("seq"); 
seq.start(sequencer);
endtask 

endclass
	  
	   
   