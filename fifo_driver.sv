 
//Write interface driver
 class fifo_driver extends ovm_driver#(fifo_sequence_item);
 fifo_sequence_item seq_item;
   virtual fifo_intf intf;
   Wrapper wrapper;
ovm_object dummy;

   
   `ovm_component_utils(fifo_driver)
   
   function new(string name = "", ovm_component parent = null);
     super.new(name, parent);
   endfunction
   
virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 


if($cast(wrapper,dummy))
begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
end
else
ovm_report_info(get_type_name(),"dummy is really dummy",OVM_LOG);

endfunction : build


virtual task run ();

reset_dut();
	forever 
		begin
	    	 seq_item_port.get_next_item(seq_item);
			 $display("i pulled the transaction");
		     $display(     $time );
			//slave_write(seq_item.addr,seq_item.data);
			//slave_write(seq_item.addr,seq_item.data);
			 write_data_task(seq_item);
			 $display(     $time );
			 $display("driver  write data is %d", intf.write_data);
             $display(     $time );
			 read_data_task(seq_item); 
			 $display(     $time );  

             $display(     $time );
	    	 seq_item_port.item_done();
		end
  
endtask : run

task write_data_task (fifo_sequence_item seq_item);
	intf.write_data = seq_item.data;
	intf.write_addr = seq_item.addr;
	$display("at driver data and address is %d %d",seq_item.data,seq_item.addr);
    //intf.wr_en = seq_item.wr_en;
	//intf.rd_en = seq_item.rd_en;
	intf.wr_en = 1;
	intf.rd_en = 0;
	#100ns
	intf.wr_en  = 0;
endtask

task read_data_task(fifo_sequence_item seq_item);
	#100ns
	intf.wr_en =  0  ;
	intf.rd_en =   1  ;
	intf.read_addr = seq_item.addr;
    @(posedge intf.clk)
	
	seq_item.data = intf.read_data; 
    $display("driver read address is %d read data is %d",seq_item.addr,seq_item.data);	
	$display($time, "intf.read_data is  = %0x,intf.", intf.read_data);
	#100ns
	intf.rd_en  = 0;
endtask : read_data_task


task reset_dut();
    intf.rst = 0;
	intf.wr_en = 0;
	intf.rd_en = 0;
    #50ns ;
    intf.rst = 1;
 endtask
endclass : fifo_driver   
